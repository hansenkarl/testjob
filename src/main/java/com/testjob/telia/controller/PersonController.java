package com.testjob.telia.controller;

import com.testjob.telia.model.Person;
import com.testjob.telia.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("api/")
@Transactional
@CrossOrigin(origins = "http://localhost:3000")
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    @GetMapping("person")
    public List<Person> getPersons() {
        return personRepository.findAll();
    }

    @PostMapping("person")
    public Person createPerson(@RequestBody Person person) {
        return personRepository.save(person);
    }

    @GetMapping("person/{id}")
    public ResponseEntity<Person> getPersonById(@PathVariable Long id) {
        Optional<Person> person = personRepository.findById(id);
        if (person.isEmpty()) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(person.get());
        }
    }

    @PutMapping("/person/{id}")
    public ResponseEntity<Person> updatePerson(@PathVariable Long id, @RequestBody Person newPerson) {
        Optional<Person> person = personRepository.findById(id);
        if (person.isEmpty()) {
            return ResponseEntity.notFound().build();
        } else {
            person.get().setFirstName(newPerson.getFirstName());
            person.get().setLastName(newPerson.getLastName());
            person.get().setBirthday(newPerson.getBirthday());
            person.get().setEmail(newPerson.getEmail());
            person.get().setPhoneNr(newPerson.getPhoneNr());
            Person updatedPerson = personRepository.save(person.get());
            return ResponseEntity.ok(updatedPerson);
        }
    }

    @DeleteMapping("/person/{id}")
    public ResponseEntity<Map<String, Boolean>> deletePerson(@PathVariable Long id) {
        Optional<Person> person = personRepository.findById(id);
        if (person.isEmpty()) {
            return ResponseEntity.notFound().build();
        } else {
            ResponseEntity.ok(person.get());
            personRepository.delete(person.get());
            Map<String, Boolean> response = new HashMap<>();
            response.put("deleted", Boolean.TRUE);
            return ResponseEntity.ok(response);
        }

    }
}
