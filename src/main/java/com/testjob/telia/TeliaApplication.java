package com.testjob.telia;

import com.testjob.telia.model.Person;
import com.testjob.telia.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeliaApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(TeliaApplication.class, args);
    }

    @Autowired
    private PersonRepository personRepository;

    @Override
    public void run(String... args) {
        this.personRepository.save(new Person("Heli", "Kopter", "1998 juuni 4", "helikopter@gmail.com", "5690420"));
        this.personRepository.save(new Person("Juta", "Jänes", "1960 november 22", "jänkujuta@mail.ee", "5754856"));
        this.personRepository.save(new Person("Aadu", "Kadakas", "1970 september 13", "aadukadakas@mail.ee", "5435823"));
    }
}
