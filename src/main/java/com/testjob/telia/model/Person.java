package com.testjob.telia.model;

import lombok.*;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Table(name = "person")
@Data
public class Person {
    @Id
    @GeneratedValue
    public Long id;
    @NonNull
    @Column(name = "first_name")
    public String firstName;
    @NonNull
    @Column(name = "last_name")
    public String lastName;
    @NonNull
    @Column(name = "birthday")
    public String birthday;
    @NonNull
    public String email;
    @NonNull
    @Column(name = "phone_nr")
    public String phoneNr;
}
