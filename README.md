Backendi käivitamiseks:
- käsurealt jooksutada kaustas testjob käsk: `gradlew bootrun`
- Intellij IDEA valida Gradle alt telia -> Tasks -> application -> `bootrun`
- aadressil http://localhost:8080/api/person on andmebaasis olevad andmed

Frontendi käivitamiseks:
- kaustas frontend jooksutada käsk: `npm install`
- kaustas frontend jooksutada käsk: `npm start`
- aadressil http://localhost:3000/ asub rakenduse frontend pool, kus on võimalik:
 1) vaadata andmebaasis olevaid inimesi,
 2) inimesi juurde lisada, 
 3) inimeste andmeid uuendada,
 4) inimesi andmebaasist kustutada
 5) sorteerida andmete järgi inimesi
 6) otsida mingi tunnuse järgi andmebaasis olevaid inimesi

Docker:
enne docker image'i ehitamist kasutada vastavaid käske:
- kaustas testjob `gradlew bootJar`
- kaustas frontend `npm run build`
docker image'i ehitamiseks ja kasutusele võtmiseks kasutada vastavaid käske:
- kaustas testjob `docker build -t karlhansen/testjob_back:v1 .` ja peale seda `docker run -d -p 8080:8080 karlhansen/testjob_back:v1`. Spring boot rakendus jookseb http://localhost:8080/api/person
- kaustas frontend `docker build -t karlhansen/testjob_front:v1 .` ja peale seda `docker run -d -p 3000:3000 karlhansen/testjob_front:v1`. React frontend jookseb http://localhost:3000/ 