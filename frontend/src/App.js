import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import HeaderComponent from "./components/HeaderComponent";
import FooterComponent from "./components/FooterComponent";
import CreateOrUpdatePersonComponent from "./components/CreateOrUpdatePersonComponent";
import PersonComponent from "./components/PersonComponent";

function App() {
    return (
        <Router>
            <HeaderComponent/>
            <div className="container position-relative">
                <Switch>
                    <Route path="/" exact component={PersonComponent}/>
                    <Route path="/add-person/:id" component={CreateOrUpdatePersonComponent}/>
                </Switch>
            </div>
            <FooterComponent/>
        </Router>
    );
}

export default App;
