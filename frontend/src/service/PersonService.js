import axios from 'axios'

const PERSON_REST_API_URL = 'http://localhost:8080/api/person';

class PersonService {
    getPersons() {
        return axios.get(PERSON_REST_API_URL);
    }

    createPerson(person) {
        return axios.post(PERSON_REST_API_URL, person);
    }

    getPersonById(id) {
        return axios.get(PERSON_REST_API_URL + '/' + id);
    }

    updatePerson(person, id) {
        return axios.put(PERSON_REST_API_URL + '/' + id, person);
    }

    deletePerson(id) {
        return axios.delete(PERSON_REST_API_URL +"/" + id);
    }
}

export default new PersonService();