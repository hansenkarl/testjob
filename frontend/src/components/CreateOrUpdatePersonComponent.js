import React, {Component} from 'react'
import PersonService from "../service/PersonService";

class CreateOrUpdatePersonComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.match.params.id,
            firstName: '',
            lastName: '',
            birthday: '',
            email: '',
            phoneNr: ''
        }
    }

    componentDidMount() {
        if (this.state.id === "add") {
        } else {
            PersonService.getPersonById(this.state.id).then((response) => {
                let person = response.data;
                this.setState({
                    firstName: person.firstName,
                    lastName: person.lastName,
                    birthday: person.birthday,
                    email: person.email,
                    phoneNr: person.phoneNr
                });
            });
        }
    }

    onChange = event => {
        const {name, value} = event.target;
        this.setState({
            [name]: value
        })
    }

    saveOrUpdatePerson = (event) => {
        event.preventDefault();
        let person = {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            birthday: this.state.birthday,
            email: this.state.email,
            phoneNr: this.state.phoneNr
        }
        if (this.state.id === 'add') {
            PersonService.createPerson(person).then(() => {
                this.props.history.push("/");
            });
        } else {
            PersonService.updatePerson(person, this.state.id).then(() => {
                this.props.history.push('/')
            });
        }

    }

    cancel() {
        this.props.history.push("/");
    }

    getFormTitle() {
        if (this.state.id === 'add') {
            return <h3 className="text-center">Add person</h3>;
        } else {
            return <h3 className="text-center">Update person</h3>;
        }
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="card col-md-6 offset-md-3">
                        {this.getFormTitle()}
                        <div className="card-body">
                            <form>
                                <div className="form-group">
                                    <label>First name</label>
                                    <input placeholder="First name" name="firstName" className="form-control"
                                           value={this.state.firstName} onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                    <label>Last name</label>
                                    <input placeholder="Last name" name="lastName" className="form-control"
                                           value={this.state.lastName} onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                    <label>Birthday</label>
                                    <input placeholder="Birthday" name="birthday" className="form-control"
                                           value={this.state.birthday} onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                    <label>Email address</label>
                                    <input placeholder="Email address" name="email" className="form-control"
                                           value={this.state.email} onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                    <label>Phone nr</label>
                                    <input placeholder="Phone nr" name="phoneNr" className="form-control"
                                           value={this.state.phoneNr} onChange={this.onChange}/>
                                </div>
                                <button className="btn btn-success" onClick={this.saveOrUpdatePerson}>Save</button>
                                <button className="btn btn-danger" onClick={this.cancel.bind(this)}
                                        style={{marginLeft: "1rem"}}>Cancel
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CreateOrUpdatePersonComponent;