import React, {useEffect, useState} from 'react';
import PersonService from "../service/PersonService";
import {useTable} from "react-table";

function PersonComponent(props) {
    const [downloadedData, setDownloadedData] = useState([{}]);
    // null = don't sort, string = person[key]
    const [sortKey, setSortKey] = useState(null);
    // 1 = up, -1 = down
    const [sortingDirection, setSortingDirection] = useState(1);
    const [searchString, setSearchString] = useState('');

    function editPerson(id) {
        props.history.push(`/add-person/${id}`);
    }

    function deletePerson(id) {
        PersonService.deletePerson(id).then(() => {
            setDownloadedData(downloadedData.filter(person => person.id !== id));
        })
    }

    function onChangeSorting(keyToSortBy) {
        if (keyToSortBy === "actions") {
            return;
        }
        if (keyToSortBy === sortKey) {
            if (sortingDirection === 1) {
                setSortingDirection(-1);
            } else {
                setSortKey(null);
            }
        } else {
            setSortKey(keyToSortBy);
            setSortingDirection(1);
        }
    }

    function onChangeSearch(event) {
        const text = event.target.value;
        setSearchString(text);
    }

    useEffect(() => {
        PersonService.getPersons().then((response) => {
            if (response.data !== undefined) {
                setDownloadedData(response.data)
            }
        });
    }, [])

    function containsSearched(person, searched) {
        searched = searched?.toLowerCase().trim();

        function compare(value) {
            return value !== undefined && value.toLowerCase().includes(searched)
        }

        return (
            searched === undefined ||
            compare(person.firstName) ||
            compare(person.lastName) ||
            compare(person.birthday) ||
            compare(person.email) ||
            compare(person.phoneNr)
        );
    }

    const sortedAndFiltered = React.useMemo(
        () => {
            const result = [];
            downloadedData.forEach(person => {
                if (containsSearched(person, searchString)) {
                    result.push(person)
                }
            })
            if (sortKey == null) {
                return result;
            }
            result.sort((person1, person2) => {
                const a = person1[sortKey].toLowerCase();
                const b = person2[sortKey].toLowerCase();
                if (a < b) {
                    return -1 * sortingDirection;
                } else if (a > b) {
                    return sortingDirection;
                } else {
                    return 0;
                }
            });
            return result;
        },
        [downloadedData, sortKey, sortingDirection, searchString]
    )

    const columns = React.useMemo(
        () => [
            {
                Header: 'first name',
                accessor: 'firstName',
            },
            {
                Header: 'last name',
                accessor: 'lastName',
            },
            {
                Header: 'birthday',
                accessor: 'birthday',
            },
            {
                Header: 'email',
                accessor: 'email',
            },
            {
                Header: 'phone number',
                accessor: 'phoneNr',
            },
            {
                Header: '',
                accessor: 'actions',
                Cell: (props) => (
                    <div className="tableActions">
                        <button onClick={() => editPerson(props.cell.row.original.id)} className="btn btn-info">
                            Update
                        </button>
                        <button onClick={() => deletePerson(props.cell.row.original.id)} className="btn btn-danger"
                                style={{marginLeft: "1rem"}}>
                            Delete
                        </button>
                    </div>
                )
            },
        ],
        [downloadedData]
    )

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
    } = useTable({columns, data: sortedAndFiltered})

    return (
        <table {...getTableProps()} className="table table-striped table-bordered">
            <thead>
            {headerGroups.map(headerGroup => (
                <tr {...headerGroup.getHeaderGroupProps()}>
                    {headerGroup.headers.map(column => (
                        column.id !== 'actions' ?
                            <th className="sortable" {...column.getHeaderProps()}
                                onClick={() => onChangeSorting(column.id)}>
                                <div>
                                    {column.render('Header')}
                                    {column.id === sortKey && (sortingDirection === 1 ? " ↑" : " ↓")}
                                </div>
                            </th> :
                            <th key="searchForm">
                                <input placeholder="search"
                                       className="form-control"
                                       value={searchString}
                                       onChange={(event) => onChangeSearch(event)}/>
                            </th>
                    ))}
                </tr>
            ))}
            </thead>
            <tbody {...getTableBodyProps()}>
            {rows.map(row => {
                prepareRow(row)
                return (
                    <tr {...row.getRowProps()}>
                        {row.cells.map(cell => {
                            return (
                                <td {...cell.getCellProps()}>
                                    {cell.render('Cell')}
                                </td>
                            )
                        })}
                    </tr>
                )
            })}
            </tbody>
        </table>
    )
}

export default PersonComponent;