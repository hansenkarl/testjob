import React, {Component} from 'react'
import {Link} from 'react-router-dom';

class HeaderComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <header className="App-header bg-dark">
                <h1>Manage people</h1>
                <div className="navbar">
                    <Link to="/" className="btn btn-dark">View list</Link>
                    <Link to="/add-person/add" className="btn btn-dark">Add person</Link>
                </div>
            </header>
        )
    }
}

export default HeaderComponent;