import React, {Component} from 'react'

class FooterComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    render() {
        return (
            <footer className="App-footer bg-dark">
                <p>Created by Karl Hansen</p>
            </footer>
        )
    }
}

export default FooterComponent;